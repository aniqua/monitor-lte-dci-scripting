#!/usr/bin/perl -w
#
# Copyright (c) 2008-2022 University of Utah and the Flux Group.
# 
# {{{GENIPUBLIC-LICENSE
# 
# GENI Public License
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and/or hardware specification (the "Work") to
# deal in the Work without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Work, and to permit persons to whom the Work
# is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Work.
# 
# THE WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS
# IN THE WORK.
# 
# }}}
#
use forks;
use strict;
use English;
use Getopt::Std;
use Data::Dumper;
use Date::Parse;
use POSIX qw(strftime);
use JSON;
# For a simple manifest parser.
use XML::LibXML;
use XML::LibXML::XPathContext;
use XML::LibXML::NodeList;
use Scalar::Util qw(looks_like_number);
#use PerlIO::Util;
use File::Tee qw(tee);
use Date::Parse;


#
#
sub usage()
{
    print STDERR "Usage: runmonitor.pl [-d] ".
	"[-m profile] [-c count] -p pid [-s savedir] [-w wait] <jsonfile>\n";
    print STDERR "Options:\n";
    print STDERR "  -d          - Turn on lots of debugging output\n";
    print STDERR "  -n          - No stop\n";
    print STDERR "  -c count    - Number of times to scan (at once an hour)\n";
    print STDERR "  -m pid,name - Project/Name of the monitor profile\n";
    print STDERR "  -p pid      - Project to create new experiments in\n";
    print STDERR "  -s savedir  - Directory to save data to\n";
    print STDERR "  -w wait     - Seconds to wait before starting\n";
    print STDERR "jsonfile      - Where to run the monitor. See repo\n";
    exit(1);
}
my $optlist   = "dnup:c:m:s:w:";
my $debug     = 0;
my $nostop    = 0;
my $pickup;
my $wait;
my $use_exp_duration;


#
# Configure variables
#
my $HOME             = $ENV{"HOME"};
my $BINDIR           = "$HOME/.local/bin";
my $STARTEXP         = "$BINDIR/startExperiment";
my $TERMINATEEXP     = "$BINDIR/terminateExperiment";
my $EXPSTATUS        = "$BINDIR/experimentStatus";
my $EXPMANIFESTS     = "$BINDIR/experimentManifests";
my $TAR		         = "/usr/bin/tar";
my $GENIGET          = "/usr/bin/geni-get";
my $PROFILE          = "NRDZ,monitor-lte-dci";
my $REFSPEC;
my $SAVEDIR	         = "/LTE1/srsue";
my $LOOPDELAY        = 120;
my $EXPDELAY         = 60;
my $LOOPS            = 1;
my $PID              = "NRDZ";
my $DURATION         = 3600;
my $STARTUPTIME      = 600;
my $LOGFILE          = "/tmp/monitor.$$";
my $QUERYINTERVAL    = 300;


# These are constants from protogeni/xmlrpc/GeniResponse.pm
sub GENIRESPONSE_SUCCESS()        { 0; }
sub GENIRESPONSE_BADARGS()        { 1; }
sub GENIRESPONSE_ERROR()          { 2; }
sub GENIRESPONSE_FORBIDDEN()      { 3; }
sub GENIRESPONSE_TOOBIG()         { 6; }
sub GENIRESPONSE_REFUSED()        { 7; }
sub GENIRESPONSE_SEARCHFAILED()   {12; }
sub GENIRESPONSE_BUSY()           {14; }
sub GENIRESPONSE_NETWORK_ERROR()  {35; }


# un-taint path
$ENV{'PATH'} = '/bin:/usr/bin:/usr/local/bin:/usr/site/bin';
delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};

# Protos
sub RunMonitor($$$$$$$);
sub GetStatus($$$$);
sub Wait($$$$);
sub WaitForData($$$$$);
sub Finished($$);
sub Terminate($$);
sub GetHostname($$);
sub GetNewData($$$$);
sub ReadJsonFile($);
sub fatal($);
sub GetBindings($);
sub GetDuration($);
sub GetStartTime($);
sub GetEndTime($);
sub Notify($);


sub logit($)
{
    my ($msg) = @_;
    my $stamp = POSIX::strftime("20%y-%m-%d %H:%M:%S", localtime());

    print "$stamp: $msg\n";
}

#
# Turn off line buffering on output
#
$| = 1; 

my %options = ();
if (! getopts($optlist, \%options)) {
    usage();
}
if (defined($options{"d"})) {
    $debug = 1;
}
if (defined($options{"n"})) {
    $nostop = 1;
}
if (defined($options{"c"})) {
    $LOOPS = $options{"c"};
}
if (defined($options{"m"})) {
    $PROFILE = $options{"m"};
}
if (defined($options{"p"})) {
    $PID = $options{"p"};
}
if (defined($options{"s"})) {
    $SAVEDIR = $options{"s"};
}
if (defined($options{"u"})) {
    $use_exp_duration = 1;
}

if (defined($options{"w"})) {
    $wait = $options{"w"};
}
elsif ($ENV{'USER'} ne "aniqua") {
    usage();
}



#open(STDOUT, ">> $LOGFILE") or
#die("opening $LOGFILE for STDOUT: $!");
#open(STDERR, ">> $LOGFILE") or
#die("opening $LOGFILE for STDERR: $!");

tee(STDOUT, '>', $LOGFILE);
tee(STDERR, '>', $LOGFILE);

if (defined($wait)) {
    sleep($wait);
}


#
# We need the local XMLRPC cert/key to talk to the server.
# This cert is also available from the Portal web interface, if you want
# to run this code on your own desktop.
#
if (! -e "$HOME/.ssl/emulab.pem") {
    if (! -e "$HOME/.ssl") {
	if (!mkdir("$HOME/.ssl", 0750)) {
	    fatal("Could not mkdir $HOME/.ssl: $!");
	}
    }
    system("$GENIGET rpccert > $HOME/.ssl/emulab.pem");
    if ($?) {
	fatal("Could not geni-get xmlrpc cert/key");
    }
}

#
# To-do: Wait till a specefic time
#

my $pmsg;
while ($LOOPS) {
	GetNewData("mon-1648901196", "cellsdr1-hospital-host.mon-1648901196.NRDZ.emulab.net", "hospital", \$pmsg);
	GetNewData("mon-1648901136", "cellsdr1-bes-host.mon-1648901136.NRDZ.emulab.net", "bes", \$pmsg);
	GetNewData("mon-1648901016", "nuc1.mon-1648901016.nrdz-PG0.madsen.powderwireless.net", "madsen", \$pmsg);
	GetNewData("mon-1648900656", "nuc1.mon-1648901016.nrdz-PG0.humanities.powderwireless.net", "humanities", \$pmsg);
	GetNewData("mon-1648900716", "nuc1.mon-1648901016.nrdz-PG0.ebc.powderwireless.net", "ebc", \$pmsg);
	GetNewData("mon-1648900836", "nuc1.mon-1648901016.nrdz-PG0.cpg.powderwireless.net", "cpg", \$pmsg);
	GetNewData("mon-1648900896", "nuc1.mon-1648901016.nrdz-PG0.guesthouse.powderwireless.net", "guesthouse", \$pmsg);
	GetNewData("mon-1648900956", "nuc1.mon-1648901016.nrdz-PG0.moran.powderwireless.net", "moran", \$pmsg);
    sleep(1200)
	    if ($LOOPS);
}
exit(0);

#
# Run the monitor on one node at an endpoint.
#
sub RunMonitor($$$$$$$)
{
	# Random wait
	
    my ($urn, $node_id, $bindings, $exp_duration, $start, $end, $wait) = @_;

	print "Waiting for = '$wait'\n" if ($debug);
    sleep($wait);

    my $name = "mon-" . time();
    my $refspec = (defined($REFSPEC) ? "--refspec='$REFSPEC'" : "");
    my $type;
    my $errmsg;
    my $blob;

    if (defined($pickup)) {
	$name = $pickup;
    }

    #
    # XXX Need to infer the type from the node_id.
    #
    if ($node_id =~ /nuc/ || $node_id =~ /^ed/) {
	$type = "B210";
    }
    elsif ($node_id =~ /^(cbrs|cell)/ || $node_id =~ /x310/i) {
	$type = "X310";
    }
    else {
	fatal("Could not determine radio type from $node_id");
    }
    if (!defined($pickup)) {
		my $command = "$STARTEXP -s -P -p $PID --bindings='$bindings' --name='$name'";
		if(defined($start)) {
			$command = "$command "."--start='$start'";
		}
		if(defined($use_exp_duration)){
			$end = time() + $exp_duration;
			$command = "$command "."--stop='$end'";
		}
		elsif(defined($end)) {
			$command = "$command "."--stop='$end'";
			$exp_duration = $end - time();
		}
		$command = "$command "."$refspec $PROFILE";
		logit("Starting $name");
		print "Running: '$command'\n" if ($debug);
		system($command);
		if ($?) {
		    #
		    # Issue the termination just in case. This is harmless.
		    #
		    logit("Terminating $name after start failure");

		    Notify("Could not start experiment on "."$node_id");
		
		    if (Terminate($name, \$errmsg)) {
			logit("Experiment $name could not be terminated: $errmsg");
		    }
		    return -1;
		}
    }
    logit("Waiting for $name monitor to finish");
    my $rval = Wait($name, \$blob, \$errmsg, $exp_duration);
    if (defined($pickup)) {
		if ($rval < 0) {
		    fatal($errmsg);
		}
		exit(0);
    }
    if ($rval < 0) {
	my $uuid = $blob->{'uuid'};
	my $msg  = "Experiment $name ($uuid) failure";

	if (defined($errmsg)) {
	    logit("Wait error: $errmsg");
	}
	
	#
	# Need to notify here.
	#
	if ($blob->{'status'} eq "failed") {
	    if (exists($blob->{'failure_code'})) {
		$msg .= ": exit code: " . $blob->{'failure_code'};
	    }
	}
	else {
	    $msg .= ": monitor failure";
	}
	logit($msg);
	if ($nostop) {
	    logit("Not terminating experiment in debug mode");
	}
	elsif (Terminate($name, \$errmsg)) {
	    logit("Experiment $name could not be terminated: $errmsg");
	}
	return -1;
    }
    #
    # Terminate the experiment and wait for the tarball to appear.
    #
    logit("Terminating $name after successfull run");
    if (Terminate($name, \$errmsg)) {
	logit("Experiment $name could not be terminated: $errmsg");
	return -1;
    }
    return 0;
}

#
# Wait for the experiment to finish or fail. Or just take too long.
#
sub Wait($$$$)
{
    my ($name, $pref, $pmsg, $exp_duration) = @_;
    my $refresh = 0;
    my $tries   = int($exp_duration/$QUERYINTERVAL) + 5;
    my ($hostname, $domain, $subdir);

    while ($tries) {
	my $rval = GetStatus($name, $refresh, $pref, $pmsg);
	return $rval
	    if ($rval < 0);

	#
	# Once the experiment is ready we can start trying to get
	# data. To do that we need the hostname.
	#
	#print "$tries: ".$$pref->{"status"}."\n";
	if ($$pref->{"status"} eq "ready") {
	    if (!defined($hostname)) {
		($hostname, $domain) = GetHostname($name, $pmsg);
		return -1
		    if (!defined($hostname));

		if ($domain eq "emulab.net") {
			$subdir = (split /\-/, "$hostname")[1];
		}
		else {
			$subdir = $domain;
			$subdir =~ s/\..*$//;
		}
		

		if ($debug) {
		    print "$hostname, $domain, $subdir\n";
		}
	    }
	    if (GetNewData($name, $hostname, $subdir, $pmsg) != 0) {
		if ($debug) {
		    print STDERR $$pmsg . "\n";
		}
	    }
	}

	$rval = Finished($$pref, $pmsg);
	return $rval
	    if ($rval < 0 || $rval > 0);
	
	$tries--;
	sleep($QUERYINTERVAL)
	    if ($tries);
    }
    $$pmsg = "timed out waiting for monitor to finish";
    return -1;
}

#
# Grab new data.
#
sub GetNewData($$$$)
{
    my ($name, $hostname, $subdir, $pmsg) = @_;
    my $hosturl = "http://${hostname}:7998";
    my $json    = "";

    #
    # Grab the listing, which is a json structure with a bunch of
    # filenames in it.
    #
    my $command = "wget -q -O - $hosturl/listing.php";
    if ($debug) {
	print "$command\n";
    }
    if (open(WGET, "$command |")) {
	while (<WGET>) {
	    $json .= $_;
	}
	close(WGET);
    }
    else {
	print STDERR "$command failed\n";
	return -1;
    }
    if ($debug) {
	print "$json\n";
    }
    my $blob = eval { decode_json($json); };
    if ($@) {
	$$pmsg = "Could not decode json data for $hostname";
	return -1;
    }
    if ($debug) {
	print Dumper($blob);
    }
    
    #
    # Grab any new files.
    #
    foreach my $ref (@$blob) {
	my $fname = $ref->{'name'};
	my $localfile  = "$SAVEDIR/$subdir/$fname";
	my $remotefile = "$hosturl/$fname";

	next
	    if (-e $localfile);
	
	print "$fname, $localfile, $remotefile\n";

	if (!-e "$SAVEDIR/$subdir" &&
	    !mkdir("$SAVEDIR/$subdir", 0755)) {
	    $$pmsg = "Could not mkdir $SAVEDIR/$subdir: $!";
	    return -1;
	}
	if ($debug) {
	    print "Getting $remotefile\n";
	}
	$command = "wget -q -O $localfile $remotefile";
	if ($debug) {
	    print "$command\n";
	}
	system("$command");
	if ($?) {
	    $$pmsg = "Failed: $command";
	    return -1;
	}
    }
    return 0;
}

#
# Check the status blob to see if the monitor ran successfully or has
# failed.
#
sub Finished($$)
{
    my ($blob, $pmsg) = @_;

    my $status = $blob->{"status"};
    if ($status eq "failed") {
	$$pmsg = "Experiment failed to setup";
	return -1;
    }
    if ($status eq "canceled") {
	$$pmsg = "Experiment has been canceled";
	return -2;
    }
    if ($status eq "ready" && exists($blob->{"execute_status"})) {
	#
	# Waiting for the execute service to finish.
	#
	my $execute_status = $blob->{"execute_status"};
	if ($execute_status->{"running"} == 0) {
	    if ($execute_status->{"failed"} != 0) {
		$$pmsg = "Execute service failed";
		return -1;
	    }
	    return 1;
	}
    }
    return 0;
}

#
# Terminate. Have to watch for boss being offline.
#
sub Terminate($$)
{
    my ($name, $pmsg) = @_;
    my $tries  = 30;
    my $blob;

    while (1) {
	my $output = `$TERMINATEEXP $PID,$name`;
	if ($?) {
	    my $status = $? >> 8;
	    if ($status == GENIRESPONSE_REFUSED() ||
		$status == GENIRESPONSE_NETWORK_ERROR()) {
		logit("$name: boss is offline, waiting for a bit");
		sleep(60);
		next;
	    }
	    elsif ($status == GENIRESPONSE_BUSY()) {
		logit("$name: experiment is busy, waiting for a bit");
		$tries--;
		sleep(30);
		next;
	    }
	    elsif ($status == GENIRESPONSE_SEARCHFAILED()) {
		logit("$name: experiment is gone");
		return 0;
	    }
	    elsif ($status != 1) {
		# Everything else is bad news. A positive error code
		# typically means we could not get to the cluster. But
		# the experiment it marked for cancel, and eventually
		# it is going to happen.
		print $output;
		$$pmsg = "unexpected error: $status";
		return -1;
	    }
	}
	last;
    }
    $tries = 30;
    # Now we wait until it really is gone.
    while ($tries) {
	my $rval = GetStatus($name, 0, \$blob, $pmsg);
	return $rval
	    if ($rval < 0);
	return 0
	    if ($rval == GENIRESPONSE_SEARCHFAILED());
	
	# Experiment is still hanging on. Wait some more.
	$tries--;
	sleep(30)
	    if ($tries);
    }
    # Experiment has not died. Need to think about this.
    $$pmsg = "timed out waiting for experiment to terminate";
    return -1;
}

#
# Experiment status. Have to watch for boss going offline and keep
# trying until it comes back online.
#
sub GetStatus($$$$)
{
    my ($name, $refresh, $pref, $pmsg) = @_;
    my $tries = 15;
    my $opts  = "-j " . ($refresh ? "-r" : "");

    print "GetStatus: $name\n" if ($debug);

    while ($tries) {
	my $output = `$EXPSTATUS $opts $PID,$name`;
	if ($?) {
	    my $status = $? >> 8;
	    if ($status == GENIRESPONSE_REFUSED() ||
		$status == GENIRESPONSE_NETWORK_ERROR()) {
		logit("$name: boss is offline, waiting for a bit");
		sleep(60);
		next;
	    }
	    elsif ($status == GENIRESPONSE_BUSY()) {
		logit("$name: experiment is busy, waiting for a bit");
		$tries--;
		sleep(30);
		next;
	    }
	    elsif ($status == GENIRESPONSE_SEARCHFAILED()) {
		return GENIRESPONSE_SEARCHFAILED();
	    }
	    else {
		print $output;
		# Everything else is bad news.
		$$pmsg = "unexpected error: $status";
		return -1;
	    }
	}
	print $output if ($debug);
	
	my $blob = eval { decode_json($output); };
	if ($@) {
	    print $output;
	    $$pmsg = "Could not decode json data for $name";
	    return -1;
	}
	# Always set this so caller has debugging info.
	$$pref = $blob;
	return 0;
    }
    $$pmsg = "status timed out\n";
    return -1;
}

sub fatal($)
{
    my ($msg) = @_;

    die("*** $0:\n".
	"    $msg\n");
}

sub XML_FindNodes($$)
{
    my ($path, $node) = @_;
    my $result = undef;
    my $ns = undef;
    
    eval {
	my $xc = XML::LibXML::XPathContext->new();
	$ns = $node->namespaceURI();
	if (defined($ns)) {
	    $xc->registerNs('n', $ns);
	}
	else {
	    $path =~ s/\bn://g;
	}
	$result = $xc->findnodes($path, $node);
    };
    if ($@) {
        die("Failed to find nodes using XPath path='$path', ns='$ns': $@\n");
    }
    return $result;
}
# Returns the first Node which matches a given XPath.
sub XML_FindFirst($$)
{
    my ($path, $node) = @_;
    return XML_FindNodes($path, $node)->pop();
}
sub XML_GetText($$)
{
    my ($name, $node) = @_;
    return $node->getAttribute($name);
}
sub GetHostname($$)
{
    my ($name, $pmsg) = @_;
    my $parser = XML::LibXML->new;
    my $doc;
    my $domain;

    #
    # Need to ask for the manifest.
    #
    my $command = "$EXPMANIFESTS $PID,$name";
    if ($debug) {
	print "$command\n";
    }
    my $json = `$command`;
    if ($?) {
	$$pmsg = "Failed: '$command'";
	return undef;
    }
    chomp($json);

    # We get an array (by urn) back.
    my $blob = eval { decode_json($json); };
    if ($@) {
	$$pmsg = "Could not decode json data for $name";
	return undef;
    }
    #
    # The blob is a hash with urn as the key, but this experiment has
    # only a single urn and that is the one we want.
    #
    my $urn = (keys(%{$blob}))[0];
    my $xml = $blob->{$urn};
    if (!defined($xml)) {
	$$pmsg = "Cannot find manifest in the blob";
	return undef;
    }
    if ($urn =~ /^urn:publicid:IDN\+([\w\.\-]+)\+authority/i) {
	$domain = $1;
    }
    else {
	$$pmsg = "Cannot parse urn: $urn\n";
	return undef;
    }
    
    eval {
	$doc = $parser->parse_string($xml);
    };
    if ($@) {
	$$pmsg = "Failed to parse xml string: $@";
	return undef;
    }
    my $root = $doc->documentElement();
    my $nodes = XML_FindNodes("n:node", $root);
    if (!$nodes) {
	$$pmsg = "Cannot find any nodes in the manifest\n";
	return undef;
    }
    foreach my $ref ($nodes->get_nodelist()) {
	my $client_id = XML_GetText("client_id", $ref);
	if (!defined($client_id)) {
	    $$pmsg = "Cannot find client_id";
	    return undef;
	}

	# Find the host element.
	my $host = XML_FindFirst("n:host", $ref);
	if (!defined($client_id)) {
	    $$pmsg = "Cannot find host element";
	    return undef;
	}
	my $hostname = XML_GetText("name", $host);
	my $ipv4 = XML_GetText("ipv4", $host);
	if (!defined($ipv4)) {
	    # No IP is best indicator that it is a radio not the host :-)
	    next;
	}
	return ($hostname, $domain);
    }
    return undef;
}

#
# Read in the json file of radios to scan
#
sub ReadJsonFile($)
{
    my ($jsonfile) = @_;
    my $json = "";
    
    fatal("json file does not exist")
	if (! -e $jsonfile);

    open(L, $jsonfile)
	or fatal("Could not open $jsonfile for reading");

    while (<L>) {
	$json .= $_;
    }
    close(L);
    fatal("json file cannot be read")
	if ($json eq "");

    my $new = eval { from_json($json, {"relaxed" => 1}); };
    if ($@) {
	fatal($@);
    }
    if (!defined($new)) {
	fatal("Invalid json file");
    }
    return $new;
}

#
# Construct bindings from a json object
#
sub GetBindings($)
{
	my ($radio) = @_;
	my $bindings = '{';
	foreach my $key(keys %$radio) {
    	#print "$key, $radio->{$key}\n";
    	if ($key eq "Start" or $key eq "End") {
    		next;
    	}
    	$bindings = $bindings.'"'.$key.'"'.': '; 
    	if (looks_like_number($radio->{$key})) {
		    $bindings = $bindings.$radio->{$key}; 
		}
		else {
			$bindings = $bindings.'"'.$radio->{$key}.'"'; 
		}
		$bindings = $bindings.', '; 
	}
	chop($bindings);
	chop($bindings);
	$bindings = $bindings.'}'; 
	return $bindings;
}

#
# Get experiment start time
#
sub GetStartTime($)
{
	my ($radio) = @_;
	my $start;
	foreach my $key(keys %$radio) {
    	#print "$key, $radio->{$key}\n";
    	if ($key eq "Start") {
    		$start = str2time($radio->{$key});
    		last;
    	}
	}
	return $start;
}

#
# Get experiment end time
#
sub GetEndTime($)
{
	my ($radio) = @_;
	my $end;
	foreach my $key(keys %$radio) {
    	#print "$key, $radio->{$key}\n";
    	if ($key eq "End") {
    		$end = str2time($radio->{$key});
    		last;
    	}
	}
	return $end;
}

#
# Get experiment duration from a json object
#
sub GetDuration($)
{
	my ($radio) = @_;

	if (exists($radio->{'RunCount'}) && exists($radio->{'Duration'}) && exists($radio->{'Channels'})) {

		my @channels = split(',', $radio->{'Channels'});
		my $c = @channels;

	    my $mult = 1;
	    my $t = $radio->{'Duration'};
	    if ($t=~ m/m$/) {
	    	chop($t);
        	$mult = 60;
	    }
	    elsif ($t=~ m/h$/) {
	    	chop($t);
        	$mult = 60*60;
	    }
	    elsif ($t=~ m/s$/) {
	    	chop($t);
        	$mult = 1;
	    }
	    my $exp_t = $t*$mult + 5;
	    
	    return $radio->{'RunCount'}*$exp_t*$c + $STARTUPTIME;
	}
	else { 
		return $DURATION;
	}
}

sub Notify($)
{
    my ($mesg) = $_[0];

    if (! -t) {
	SENDMAIL("aniqua\@cs.utah.edu",
		 "LTE DCI Monitoring Experiment",
		 $mesg . "\n\n", undef,
		 "",
		 $LOGFILE);
	# So it has time to depart before experiment termination.
	sleep(10);
    }
    
}