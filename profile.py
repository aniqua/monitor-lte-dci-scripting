"""
This profile will periodically run the RF monitor on a set of radios
using the XMLRPC interface to the Powder control framework. The set of
radios is specified in a json file, there is an example in the repo. A
new experiment is started for each radio, the monitor is run once, the
data file is copied back, and the experiment is terminated. 

This profile is instantiated on a single generic compute node, it
does not need to be anything fancy, since it all it does is invoke
the XMLRPC interface and copy back the data files. 

Instructions:
Once your node boots, log in and make a copy of the json file:
```
        cp /local/repository/radios.json /tmp/radios.json
```
Edit this file, you will of course want to choose radios/nodes you are
allowed to use. If you choose a node that is not in service, it will be
skipped and an error generated. Then start the driver script:
```
        /local/repository/runmonitor.pl -p PID /tmp/radios.json
```
where the PID is a project name you are a member of and has permission to use
the radios. The script runs in the foreground and sends status info to
STDOUT/STDERR.

The data files are collected from the target radios (compute nodes)
and stored in `/local/www/` on your node. You can browse the
[result frequency graphs](http://{host-node}:7998/frequency-graphs.html).
You will need to refresh the page occasionally to see newly added graphs. 
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Emulab specific extensions.
import geni.rspec.emulab as emulab


computeTypes = [
    ('Any', 'Any'),
    ('d740', 'd740'),
    ('d430', 'd430'),
    ('d820', 'd820'),
]
datasets = [
    ("None", "None"),
    ("urn:publicid:IDN+emulab.net:nrdz+ltdataset+SpectSet", "SpectSet"),
    ("urn:publicid:IDN+emulab.net:nrdz+ltdataset+LTE1", "LTE1"),
    ("urn:publicid:IDN+emulab.net:nrdz+ltdataset+LTE2", "LTE2"),
]

# Helper function to connect to a dataset
def connect_to_dataset(node, dataset):
    # We need a link to talk to the remote file system, so make an interface.
    iface = node.addInterface()

    # The remote file system is represented by special node.
    fsnode = request.RemoteBlockstore("fsnode", "/" + dataset.split('+')[-1])

    # This URN is displayed in the web interfaace for your dataset.
    fsnode.dataset = dataset
    #
    # The "rwclone" attribute allows you to map a writable copy of the
    # indicated SAN-based dataset. In this way, multiple nodes can map
    # the same dataset simultaneously. In many situations, this is more
    # useful than a "readonly" mapping. For example, a dataset
    # containing a Linux source tree could be mapped into multiple
    # nodes, each of which could do its own independent,
    # non-conflicting configure and build in their respective copies.
    # Currently, rwclones are "ephemeral" in that any changes made are
    # lost when the experiment mapping the clone is terminated.
    #
    fsnode.rwclone = False

    #
    # The "readonly" attribute, like the rwclone attribute, allows you to
    # map a dataset onto multiple nodes simultaneously. But with readonly,
    # those mappings will only allow read access (duh!) and any filesystem
    # (/mydata in this example) will thus be mounted read-only. Currently,
    # readonly mappings are implemented as clones that are exported
    # allowing just read access, so there are minimal efficiency reasons to
    # use a readonly mapping rather than a clone. The main reason to use a
    # readonly mapping is to avoid a situation in which you forget that
    # changes to a clone dataset are ephemeral, and then lose some
    # important changes when you terminate the experiment.
    #
    fsnode.readonly = False
    
    # Now we add the link between the node and the special node
    fslink = request.Link("fslink")
    fslink.addInterface(iface)
    fslink.addInterface(fsnode.interface)

    # Special attributes for this link that we must use.
    fslink.best_effort = True
    fslink.vlan_tagging = True


# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

pc.defineParameter("ComputeType", "Compute Type",
                   portal.ParameterType.STRING, computeTypes[0], computeTypes,
                   longDescription="Select a type for the host")

pc.defineParameter("Dataset", "Dataset to connect",
                   portal.ParameterType.STRING, datasets[0], datasets,
                   longDescription="Name of the remote dataset to connect")

params = pc.bindParameters()

pc.verifyParameters()

# Create node
node = request.RawPC("node");
if params.ComputeType == "Any":
    node.hardware_type = "powder-compute"
else:
    node.hardware_type = params.ComputeType
    pass
if params.Dataset != "None":
	connect_to_dataset(node, params.Dataset)

#
# Install and start X11 VNC. Calling this informs the Portal that you want
# a VNC option in the node context menu to create a browser VNC client.
#
node.startVNC()

#
# Install support code.
#
node.addService(
    pg.Execute(shell="sh", command="/local/repository/install.sh"))

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
